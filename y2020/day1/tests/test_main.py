import os

from y2020.day1.main import extract_entries, get_part1_answer, get_part2_answer

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")


def test_extract_depths_len():
    assert len(extract_entries(filename=INPUT_FILENAME)) == 6


def test_get_part1_answer():
    entries = extract_entries(filename=INPUT_FILENAME)
    assert get_part1_answer(entries=entries, target_sum=2020) == 514579


def test_get_part2_answer():
    entries = extract_entries(filename=INPUT_FILENAME)
    assert get_part2_answer(entries=entries, target_sum=2020) == 241861950
