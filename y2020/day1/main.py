import logging
import os
import sys
from typing import List

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")
LOG_FILENAME = os.getenv("LOG_FILENAME")
LOG_FORMAT = os.getenv("LOG_FORMAT")
LOGGING_LEVEL = os.getenv("LOGGING_LEVEL")
TARGET_SUM = int(os.getenv("TARGET_SUM"))
WINDOW_SIZE = int(os.getenv("WINDOW_SIZE"))


def extract_entries(filename: str):
    entries = []
    with open(filename) as f:
        for line in f.readlines():
            try:
                scrubbed_line = line.strip("\n")
                entries.append(int(scrubbed_line))
            except ValueError:
                logging.info(f"Couldn't convert value {scrubbed_line}")
            except AttributeError:
                logging.info(f"Value of 'line' can't be {line}")
    return entries


def get_part1_answer(entries: List, target_sum: int) -> int:
    for entry in entries:
        remainder = target_sum - entry
        if remainder in entries:
            if (entry != remainder) or (entry == remainder and entries.count(remainder) >= 2):
                return entry * remainder
    return -1


def get_part2_answer(entries: List, target_sum: int) -> int:
    entries.sort()

    closest_sum = sys.maxsize

    for i in range(len(entries)-2):
        ptr1 = i + 1
        ptr2 = len(entries) - 1

        while ptr1 < ptr2:
            current_sum = entries[i] + entries[ptr1] + entries[ptr2]

            if abs(target_sum - current_sum) < abs(target_sum - closest_sum):
                closest_sum = current_sum

            if current_sum > target_sum:
                ptr2 -= 1
            elif current_sum < target_sum:
                ptr1 = ptr1 + 1
            else:
                return entries[i] * entries[ptr1] * entries[ptr2]


def main():
    logging.basicConfig(filename=LOG_FILENAME,
                        level=logging.getLevelName(LOGGING_LEVEL),
                        format=LOG_FORMAT)

    entries = extract_entries(filename=INPUT_FILENAME)
    print(f"Part 1 Answer: {get_part1_answer(entries=entries, target_sum=TARGET_SUM)}")
    print(f"Part 2 Answer: {get_part2_answer(entries=entries, target_sum=TARGET_SUM)}")


if __name__ == "__main__":
    main()
