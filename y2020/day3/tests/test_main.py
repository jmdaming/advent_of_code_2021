import os

from y2020.day3.main import extract_lines, get_part1_answer, get_part2_answer

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")


def test_extract_moves_and_boards_len():
    lines = extract_lines(filename=INPUT_FILENAME)
    assert len(lines) == 11


def test_get_part1_answer():
    lines = extract_lines(filename=INPUT_FILENAME)
    assert get_part1_answer(lines=lines) == 7


def test_get_part2_answer():
    lines = extract_lines(filename=INPUT_FILENAME)
    assert get_part2_answer(lines=lines) == 336
