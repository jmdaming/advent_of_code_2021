import logging
import math
import os
import re
from typing import List

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")
LOG_FILENAME = os.getenv("LOG_FILENAME")
LOG_FORMAT = os.getenv("LOG_FORMAT")
LOGGING_LEVEL = os.getenv("LOGGING_LEVEL")
TARGET_SUM = int(os.getenv("TARGET_SUM"))


def extract_lines(filename: str):
    lines = []
    with open(filename) as f:
        for line in f.readlines():
            try:
                scrubbed_line = line.strip("\n")
                lines.append(scrubbed_line)
            except ValueError:
                logging.info(f"Couldn't convert value {scrubbed_line}")
            except AttributeError:
                logging.info(f"Value of 'line' can't be {line}")
    return lines


def process_slope(lines: List, step_right: int, step_down: int) -> int:
    right = 0
    hits = 0
    misses = 0

    for i in range(0, len(lines), step_down):
        if 0 < right:
            val = int(math.ceil(len(lines) / step_down * step_right / len(lines[i])))
            line = lines[i] * val
            if line[right] == "#":
                hits = hits + 1
            else:
                misses = misses + 1
        right = right + step_right

    return hits


def get_part1_answer(lines: List) -> int:
    step_right = 3
    step_down = 1
    return process_slope(lines=lines, step_right=step_right, step_down=step_down)


def get_part2_answer(lines: List) -> int:
    slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
    trees = []
    for slope in slopes:
        trees.append(process_slope(lines=lines, step_right=slope[0], step_down=slope[1]))
    return math.prod(trees)


def main():
    logging.basicConfig(filename=LOG_FILENAME,
                        level=logging.getLevelName(LOGGING_LEVEL),
                        format=LOG_FORMAT)

    lines = extract_lines(filename=INPUT_FILENAME)
    print(f"Part 1 Answer: {get_part1_answer(lines=lines)}")
    print(f"Part 2 Answer: {get_part2_answer(lines=lines)}")


if __name__ == "__main__":
    main()
