import logging
import math
import os
import re
from typing import List

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")
LOG_FILENAME = os.getenv("LOG_FILENAME")
LOG_FORMAT = os.getenv("LOG_FORMAT")
LOGGING_LEVEL = os.getenv("LOGGING_LEVEL")
TARGET_SUM = int(os.getenv("TARGET_SUM"))


class Passport(object):

    def __init__(self, passport_text: str):
        elements = passport_text.split(" ")
        self.elements = {}
        for element in elements:
            elem = element.split(":")
            self.elements.update({elem[0]: elem[1]})

    def validate_part1(self) -> bool:
        required_items = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
        allow_items = ["cid"]

        return all(elem in self.elements.keys() for elem in required_items)

    def validate_year(self, element: str, lower_bound: int, upper_bound: int):
        try:
            if self.elements.get(element):
                byr = int(self.elements.get(element))
                return lower_bound <= byr <= upper_bound
        except ValueError:
            logging.info(f"Could not convert {self.elements.get('byr')} to int.")

        return False

    def validate_height(self):
        try:
            if self.elements.get("hgt"):
                found = re.search(r"(\d+)\s*(cm|in)", self.elements.get("hgt").strip())
                val = int(found.group(1))
                unit = found.group(2)

                return (unit == "in" and 59 <= val <= 76) or (unit == "cm" and 150 <= val <= 193)
        except AttributeError:
            pass
        return False

    def validate_hair_color(self):
        try:
            if self.elements.get("hcl"):
                return re.search(r"#[0-9a-f]{6}", self.elements.get("hcl").strip())
        except AttributeError:
            pass
        return False

    def validate_eye_color(self):
        acceptable_items = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
        return self.elements.get("ecl") in acceptable_items

    def validate_passport_id(self):
        try:
            if self.elements.get("pid"):
                return re.search(r"^[0-9]{9}$", self.elements.get("pid").strip())
        except AttributeError:
            pass
        return False

    def validate_part2(self):
        if not self.elements and self.validate_part1():
            return False

        return self.validate_year(element="byr", lower_bound=1920, upper_bound=2002) and \
               self.validate_year(element="iyr", lower_bound=2010, upper_bound=2020) and \
               self.validate_year(element="eyr", lower_bound=2020, upper_bound=2030) and \
               self.validate_height() and \
               self.validate_hair_color() and \
               self.validate_eye_color() and \
               self.validate_passport_id()

    def __repr__(self):
        return str(self.elements)


def extract_passports(filename: str):
    passports = []
    with open(filename) as f:
        passport_text = ""
        for line in f.readlines():
            try:
                scrubbed_line = line.strip("\n")
                if scrubbed_line:
                    if passport_text:
                        passport_text = f"{passport_text} {scrubbed_line.strip()}"
                    else:
                        passport_text = scrubbed_line.strip()
                else:
                    passports.append(Passport(passport_text=passport_text))
                    passport_text = ""
            except ValueError:
                logging.info(f"Couldn't convert value {scrubbed_line}")
            except AttributeError:
                logging.info(f"Value of 'line' can't be {line}")
        if passport_text:
            passports.append(Passport(passport_text=passport_text))
    return passports


def get_part1_answer(passports: List) -> int:
    return sum(1 if passport.validate_part1() else 0 for passport in passports)


def get_part2_answer(passports: List) -> int:
    return sum(1 if passport.validate_part2() else 0 for passport in passports)


def main():
    logging.basicConfig(filename=LOG_FILENAME,
                        level=logging.getLevelName(LOGGING_LEVEL),
                        format=LOG_FORMAT)

    passports = extract_passports(filename=INPUT_FILENAME)
    print(f"Part 1 Answer: {get_part1_answer(passports=passports)}")
    print(f"Part 2 Answer: {get_part2_answer(passports=passports)}")


if __name__ == "__main__":
    main()
