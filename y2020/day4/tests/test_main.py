import os

from y2020.day4.main import extract_passports, get_part1_answer, get_part2_answer

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")


def test_extract_moves_and_boards_len():
    passports = extract_passports(filename=INPUT_FILENAME)
    assert len(passports) == 4


def test_get_part1_answer():
    passports = extract_passports(filename=INPUT_FILENAME)
    assert get_part1_answer(passports=passports) == 2


def test_get_part2_answer():
    passports = extract_passports(filename=INPUT_FILENAME)
    assert get_part2_answer(passports=passports) == 336


def test_get_part2_invalid_passports():
    passports = extract_passports(filename="./resources/invalid_part2.txt")
    assert get_part2_answer(passports=passports) == 0


def test_get_part2_valid_passports():
    passports = extract_passports(filename="./resources/valid_part2.txt")
    assert get_part2_answer(passports=passports) == 4
