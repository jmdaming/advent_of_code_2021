import logging
import os
import re
import sys
from typing import List

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")
LOG_FILENAME = os.getenv("LOG_FILENAME")
LOG_FORMAT = os.getenv("LOG_FORMAT")
LOGGING_LEVEL = os.getenv("LOGGING_LEVEL")
TARGET_SUM = int(os.getenv("TARGET_SUM"))
WINDOW_SIZE = int(os.getenv("WINDOW_SIZE"))


class PasswordPolicy(object):

    def __init__(self, lower_bound: int, upper_bound: int, target_character: str, password: str):
        self.lower_bound = lower_bound
        self.upper_bound = upper_bound
        self.target_character = target_character
        self.password = password

    def validate_part1(self):
        return self.lower_bound <= self.password.count(self.target_character) <= self.upper_bound

    def validate_part2(self):
        return (self.password[self.lower_bound - 1] == self.target_character
                and self.password[self.upper_bound - 1] != self.target_character) or \
               (self.password[self.lower_bound - 1] != self.target_character
                and self.password[self.upper_bound - 1] == self.target_character)


def extract_passwords(filename: str):
    password_policies = []
    with open(filename) as f:
        for line in f.readlines():
            try:
                scrubbed_line = line.strip("\n")
                scrubbed_list = re.split("-|\s|:\s", scrubbed_line)
                password_policies.append(PasswordPolicy(
                    lower_bound=int(scrubbed_list[0]),
                    upper_bound=int(scrubbed_list[1]),
                    target_character=scrubbed_list[2],
                    password=scrubbed_list[3]
                ))
            except ValueError:
                logging.info(f"Couldn't convert value {scrubbed_line}")
            except AttributeError:
                logging.info(f"Value of 'line' can't be {line}")
    return password_policies


def get_part1_answer(password_policies: List) -> int:
    return sum(1 for password_policy in password_policies if password_policy.validate_part1())


def get_part2_answer(password_policies: List) -> int:
    return sum(1 for password_policy in password_policies if password_policy.validate_part2())


def main():
    logging.basicConfig(filename=LOG_FILENAME,
                        level=logging.getLevelName(LOGGING_LEVEL),
                        format=LOG_FORMAT)

    password_policies = extract_passwords(filename=INPUT_FILENAME)
    print(f"Part 1 Answer: {get_part1_answer(password_policies=password_policies)}")
    print(f"Part 2 Answer: {get_part2_answer(password_policies=password_policies)}")


if __name__ == "__main__":
    main()
