import os

from y2020.day2.main import extract_passwords, get_part1_answer, get_part2_answer

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")


def test_extract_depths_len():
    assert len(extract_passwords(filename=INPUT_FILENAME)) == 3


def test_get_part1_answer():
    password_policies = extract_passwords(filename=INPUT_FILENAME)
    assert get_part1_answer(password_policies=password_policies) == 2


def test_get_part2_answer():
    password_policies = extract_passwords(filename=INPUT_FILENAME)
    assert get_part2_answer(password_policies=password_policies) == 1
