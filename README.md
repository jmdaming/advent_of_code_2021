Advent of Code 2021
====================

This is my repo to house the source code for how I finished each daily challenge.  As context, my goal for each 
challenge was to solve it using Python in a test-drive development fashion.  

In addition, I wanted to work with building each function in a way I would for a production application (error handling, 
typing, documentation, etc.).

>### About the Challenge
>
> Advent of Code is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that 
> can be solved in any programming language you like. People use them as a speed contest, interview prep, company 
> training, university coursework, practice problems, or to challenge each other.
>
> You don't need a computer science background to participate - just a little programming knowledge and some problem 
> solving skills will get you pretty far. Nor do you need a fancy computer; every problem has a solution that completes 
> in at most 15 seconds on ten-year-old hardware.
>
> If you'd like to support Advent of Code, you can do so indirectly by helping to [Shareon Twitter Mastodon] it with 
> others, or directly via PayPal or Coinbase.
>
> Advent of Code is a registered trademark in the United States.