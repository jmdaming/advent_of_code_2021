import logging
import os
from typing import List

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")
LOG_FILENAME = os.getenv("LOG_FILENAME")
LOG_FORMAT = os.getenv("LOG_FORMAT")
LOGGING_LEVEL = os.getenv("LOGGING_LEVEL")


def extract_laternfish(filename: str) -> List:
    laternfish = []

    with open(filename) as f:
        for line in f.readlines():
            try:
                scrubbed_line = line.strip("\n")
                laternfish = [int(x) for x in scrubbed_line.split(",")]
            except ValueError:
                logging.info(f"Couldn't convert value {scrubbed_line}")
            except AttributeError:
                logging.info(f"Value of 'line' can't be {line}")

    return laternfish


def calculate_first_9_days(laternfish: List):
    first_9_days = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    laternfish.sort()
    for fish_timer in laternfish:
        first_9_days[fish_timer] = first_9_days[fish_timer] + 1
    return first_9_days


def process_day(laternfish: List):
    current_day = laternfish[0]
    updated_laternfish = laternfish[1:]
    updated_laternfish[6] = updated_laternfish[6] + current_day
    updated_laternfish.append(current_day)

    return updated_laternfish


def get_part1_answer(laternfish: List, days: int):
    laternfish = calculate_first_9_days(laternfish=laternfish)
    for i in range(days):
        laternfish = process_day(laternfish=laternfish)
    return sum(laternfish)


def get_part2_answer(laternfish: List, days: int):
    laternfish = calculate_first_9_days(laternfish=laternfish)
    for i in range(days):
        laternfish = process_day(laternfish=laternfish)
    return sum(laternfish)


def main():
    logging.basicConfig(filename=LOG_FILENAME,
                        level=logging.getLevelName(LOGGING_LEVEL),
                        format=LOG_FORMAT)

    laternfish = extract_laternfish(filename=INPUT_FILENAME)
    print(f"Part 1 Answer: {get_part1_answer(laternfish=laternfish, days=80)}")
    print(f"Part 2 Answer: {get_part2_answer(laternfish=laternfish, days=256)}")


if __name__ == "__main__":
    main()
