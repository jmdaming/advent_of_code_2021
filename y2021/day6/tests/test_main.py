import os

from y2021.day6.main import extract_laternfish, get_part1_answer, get_part2_answer

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")


def test_extract_moves_and_boards_len():
    laternfish = extract_laternfish(filename=INPUT_FILENAME)
    assert len(laternfish) == 5


def test_get_part1_answer():
    laternfish = extract_laternfish(filename=INPUT_FILENAME)
    assert get_part1_answer(laternfish=laternfish, days=80) == 5934


def test_get_part2_answer():
    laternfish = extract_laternfish(filename=INPUT_FILENAME)
    assert get_part2_answer(laternfish=laternfish, days=256) == 26984457539
