import logging
import os
from typing import List

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")
LOG_FILENAME = os.getenv("LOG_FILENAME")
LOG_FORMAT = os.getenv("LOG_FORMAT")
LOGGING_LEVEL = os.getenv("LOGGING_LEVEL")
WINDOW_SIZE = int(os.getenv("WINDOW_SIZE"))


def extract_depths(filename: str):
    depths = []
    with open(filename) as f:
        for line in f.readlines():
            try:
                scrubbed_line = line.strip("\n")
                depths.append(int(scrubbed_line))
            except ValueError:
                logging.info(f"Couldn't convert value {scrubbed_line}")
            except AttributeError:
                logging.info(f"Value of 'line' can't be {line}")
    return depths


def get_part1_answer(depths: List) -> int:
    last_depth = None
    larger_measurements = 0

    for depth in depths:
        if not last_depth:
            last_depth = depth
            continue

        if depth > last_depth:
            larger_measurements = larger_measurements + 1
        last_depth = depth

    return larger_measurements


def get_part2_answer(depths: List) -> int:
    sliding_depths = []
    for depth in depths:
        sliding_depths.append({"sum_of_measurements": depth, "count_of_measurements": 1})

        for i in range(1, WINDOW_SIZE):
            index = len(sliding_depths) - 1 - i
            if index >= 0:
                sliding_depths[index]["sum_of_measurements"] = sliding_depths[index]["sum_of_measurements"] + depth
                sliding_depths[index]["count_of_measurements"] = sliding_depths[index]["count_of_measurements"] + 1
            else:
                break

    last_sliding_sum = None
    larger_measurements = 0
    for sliding_depth in sliding_depths:
        if not last_sliding_sum:
            last_sliding_sum = sliding_depth["sum_of_measurements"]
            continue

        if sliding_depth["sum_of_measurements"] > last_sliding_sum:
            larger_measurements = larger_measurements + 1

        if sliding_depth["count_of_measurements"] != 3:
            break

        last_sliding_sum = sliding_depth["sum_of_measurements"]

    return larger_measurements


def main():
    logging.basicConfig(filename=LOG_FILENAME,
                        level=logging.getLevelName(LOGGING_LEVEL),
                        format=LOG_FORMAT)

    depths = extract_depths(filename=INPUT_FILENAME)
    print(f"Part 1 Answer: {get_part1_answer(depths=depths)}")
    print(f"Part 2 Answer: {get_part2_answer(depths=depths)}")


if __name__ == "__main__":
    main()
