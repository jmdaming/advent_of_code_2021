import os

from y2021.day1.main import extract_depths, get_part1_answer, get_part2_answer

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")


def test_extract_depths_len():
    assert len(extract_depths(filename=INPUT_FILENAME)) == 2001


def test_get_part1_answer():
    depths = extract_depths(filename=INPUT_FILENAME)
    assert get_part1_answer(depths=depths) == 1483


def test_get_part2_answer():
    depths = extract_depths(filename=INPUT_FILENAME)
    assert get_part2_answer(depths=depths) == 1519
