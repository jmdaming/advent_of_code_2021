import logging
import os
import re
from collections import Counter
from typing import List

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")
LOG_FILENAME = os.getenv("LOG_FILENAME")
LOG_FORMAT = os.getenv("LOG_FORMAT")
LOGGING_LEVEL = os.getenv("LOGGING_LEVEL")


class Coordinate(object):

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        if isinstance(other, Coordinate):
            return self.x == other.x and self.y == other.y
        return False

    def __repr__(self):
        return f"({self.x},{self.y})"

    def __hash__(self):
        return hash(f"({self.x},{self.y})")

    def compare(self, other):
        if not isinstance(other, Coordinate):
            raise ValueError(f"Can't compare type {type(self)} to {type(other)}")

        x_vector = 0
        y_vector = 0
        if self.x < other.x:
            x_vector = 1
        elif self.x > other.x:
            x_vector = -1
        if self.y < other.y:
            y_vector = 1
        elif self.y > other.y:
            y_vector = -1

        return x_vector, y_vector


class Line(object):
    def __init__(self, coord1, coord2):
        self.coord1 = coord1
        self.coord2 = coord2

    def get_all_coordinates(self, part2):
        all_coordinates = []

        if (self.coord1.x != self.coord2.x and self.coord1.y != self.coord2.y and part2) or \
                not (self.coord1.x != self.coord2.x and self.coord1.y != self.coord2.y):
            x_vector, y_vector = self.coord1.compare(self.coord2)
            all_coordinates.append(self.coord1)
            c = self.coord1
            while c != self.coord2:
                c = Coordinate(c.x + x_vector, c.y + y_vector)
                all_coordinates.append(c)

        return all_coordinates

    def __eq__(self, other):
        return (self.coord1 == other.coord1 and self.coord2 == other.coord2) or \
               (self.coord1 == other.coord2 and self.coord2 == other.coord1)

    def __repr__(self):
        return f"{self.coord1} -> {self.coord2}"


class Map(object):

    def __init__(self, lines=None):
        if lines is None:
            lines = []
        self.lines = lines

    def get_cross_sections(self, part2):
        all_coordinates = []
        for line in self.lines:
            all_coordinates.extend(line.get_all_coordinates(part2=part2))
        return Counter(all_coordinates)


def extract_lines(filename: str) -> List[Line]:
    lines = []

    with open(filename) as f:
        for line in f.readlines():
            try:
                scrubbed_line = line.strip("\n")
                scrubbed_coordinates = [elem.split(",") for elem in re.split(r"\s*->\s*", scrubbed_line.strip())]
                lines.append(
                    Line(coord1=Coordinate(x=int(scrubbed_coordinates[0][0]), y=int(scrubbed_coordinates[0][1])),
                         coord2=Coordinate(x=int(scrubbed_coordinates[1][0]), y=int(scrubbed_coordinates[1][1]))))
            except ValueError:
                logging.info(f"Couldn't convert value {scrubbed_line}")
                print(f"Couldn't convert value {scrubbed_line}")
            except AttributeError:
                logging.info(f"Value of 'line' can't be {line}")

    return lines


def get_part1_answer(lines: List):
    m = Map(lines=lines)
    c = m.get_cross_sections(part2=False)
    return len({x: count for x, count in c.items() if count >= 2})


def get_part2_answer(lines: List):
    m = Map(lines=lines)
    c = m.get_cross_sections(part2=True)
    return len({x: count for x, count in c.items() if count >= 2})


def main():
    logging.basicConfig(filename=LOG_FILENAME,
                        level=logging.getLevelName(LOGGING_LEVEL),
                        format=LOG_FORMAT)

    lines = extract_lines(filename=INPUT_FILENAME)
    print(f"Part 1 Answer: {get_part1_answer(lines=lines)}")
    print(f"Part 2 Answer: {get_part2_answer(lines=lines)}")


if __name__ == "__main__":
    main()
