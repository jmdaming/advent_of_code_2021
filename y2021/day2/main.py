from enum import Enum
import logging
import os
from typing import List

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")
LOG_FILENAME = os.getenv("LOG_FILENAME")
LOG_FORMAT = os.getenv("LOG_FORMAT")
LOGGING_LEVEL = os.getenv("LOGGING_LEVEL")


class Command(Enum):
    FORWARD = "forward"
    UP = "up"
    DOWN = "down"


def extract_commands(filename: str) -> List:
    commands = []
    with open(filename) as f:
        for line in f.readlines():
            try:
                scrubbed_line = line.strip("\n")
                command = (line.split(sep=" ")[0], int(line.split(sep=" ")[1]))
                commands.append(command)
            except ValueError:
                logging.info(f"Couldn't convert value {scrubbed_line}")
            except AttributeError:
                logging.info(f"Value of 'line' can't be {line}")
    return commands


def get_part1_helper(commands: List):
    horizontal = 0
    depth = 0

    for command in commands:
        c = command[0]
        d = int(command[1])
        if Command(c) == Command.FORWARD:
            horizontal = horizontal + d
        elif Command(c) == Command.UP:
            depth = depth - d
        elif Command(c) == Command.DOWN:
            depth = depth + d

    return {"horizontal": horizontal, "depth": depth}


def get_part1_answer(commands: List) -> int:
    position = get_part1_helper(commands=commands)
    return position["horizontal"] * position["depth"]


def get_part2_helper(commands: List):
    horizontal = 0
    depth = 0
    aim = 0

    for command in commands:
        c = command[0]
        d = int(command[1])
        if Command(c) == Command.FORWARD:
            horizontal = horizontal + d
            depth = depth + (aim * d)
        elif Command(c) == Command.UP:
            aim = aim - d
        elif Command(c) == Command.DOWN:
            aim = aim + d

    return {"horizontal": horizontal, "depth": depth, "aim": aim}


def get_part2_answer(commands: List) -> int:
    position = get_part2_helper(commands=commands)
    return position["horizontal"] * position["depth"]


def main():
    logging.basicConfig(filename=LOG_FILENAME,
                        level=logging.getLevelName(LOGGING_LEVEL),
                        format=LOG_FORMAT)

    commands = extract_commands(filename=INPUT_FILENAME)
    print(f"Part 1 Answer: {get_part1_answer(commands=commands)}")
    print(f"Part 2 Answer: {get_part2_answer(commands=commands)}")


if __name__ == "__main__":
    main()
