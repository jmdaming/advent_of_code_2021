import os

from y2021.day2.main import extract_commands, get_part1_helper, get_part1_answer, get_part2_helper, get_part2_answer

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")


def test_extract_depths_len():
    assert len(extract_commands(filename=INPUT_FILENAME)) == 6


def test_get_part1_helper():
    commands = extract_commands(filename=INPUT_FILENAME)
    position = get_part1_helper(commands=commands)
    assert position["horizontal"] == 15
    assert position["depth"] == 10


def test_get_part1_answer():
    commands = extract_commands(filename=INPUT_FILENAME)
    assert get_part1_answer(commands=commands) == 150


def test_get_part2_helper():
    commands = extract_commands(filename=INPUT_FILENAME)
    position = get_part2_helper(commands=commands)
    assert position["horizontal"] == 15
    assert position["depth"] == 60
    assert position["aim"] == 10


def test_get_part2_answer():
    commands = extract_commands(filename=INPUT_FILENAME)
    assert get_part2_answer(commands=commands) == 900
