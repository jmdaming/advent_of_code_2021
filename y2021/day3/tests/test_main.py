import os

from y2021.day3.main import extract_diagnostics, most_frequent, least_frequent, build_diagnostic_columns, build_gamma_rate, \
    build_epsilon_rate, get_part1_answer, get_part2_answer, extract_oxygen_generator_rating, extract_co2_scrubber_rating

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")
diagnostics_column = ["0", "1", "1", "1", "1", "0", "0", "1", "1", "1", "0", "0"]


def test_extract_diagnostics_len():
    assert len(extract_diagnostics(filename=INPUT_FILENAME)) == 12


def test_least_frequent():
    assert least_frequent(diagnostic_column=diagnostics_column) == "0"


def test_most_frequent():
    assert most_frequent(diagnostic_column=diagnostics_column) == "1"


def test_build_diagnostic_columns():
    diagnostics = extract_diagnostics(filename=INPUT_FILENAME)
    expected_output = [["0", "1", "1", "1", "1", "0", "0", "1", "1", "1", "0", "0"],
                       ["0", "1", "0", "0", "0", "1", "0", "1", "0", "1", "0", "1"],
                       ["1", "1", "1", "1", "1", "1", "1", "1", "0", "0", "0", "0"],
                       ["0", "1", "1", "1", "0", "1", "1", "0", "0", "0", "1", "1"],
                       ["0", "0", "0", "1", "1", "1", "1", "0", "0", "1", "0", "0"]]
    assert build_diagnostic_columns(diagnostics) == expected_output


def test_build_gamma_rate():
    diagnostics = extract_diagnostics(filename=INPUT_FILENAME)
    diagnostic_columns = build_diagnostic_columns(diagnostics=diagnostics)
    assert build_gamma_rate(diagnostic_columns=diagnostic_columns) == "10110"


def test_build_epsilon_rate():
    diagnostics = extract_diagnostics(filename=INPUT_FILENAME)
    diagnostic_columns = build_diagnostic_columns(diagnostics=diagnostics)
    assert build_epsilon_rate(diagnostic_columns=diagnostic_columns) == "01001"


def test_get_part1_answer():
    diagnostics = extract_diagnostics(filename=INPUT_FILENAME)
    assert get_part1_answer(diagnostics=diagnostics) == 198


def test_extract_oxygen_generator_rating():
    diagnostics = extract_diagnostics(filename=INPUT_FILENAME)
    assert extract_oxygen_generator_rating(diagnostics=diagnostics) == "10111"


def test_extract_co2_scrubber_rating():
    diagnostics = extract_diagnostics(filename=INPUT_FILENAME)
    assert extract_co2_scrubber_rating(diagnostics=diagnostics) == "01010"


def test_get_part2_answer():
    diagnostics = extract_diagnostics(filename=INPUT_FILENAME)
    assert get_part2_answer(diagnostics=diagnostics) == 230
