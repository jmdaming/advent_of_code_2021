import logging
import os
from collections import Counter
from typing import List, Callable

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")
LOG_FILENAME = os.getenv("LOG_FILENAME")
LOG_FORMAT = os.getenv("LOG_FORMAT")
LOGGING_LEVEL = os.getenv("LOGGING_LEVEL")


def extract_diagnostics(filename: str) -> List:
    diagnostics = []
    with open(filename) as f:
        for line in f.readlines():
            try:
                scrubbed_line = line.strip("\n")
                diagnostics.append(scrubbed_line)
            except ValueError:
                logging.info(f"Couldn't convert value {scrubbed_line}")
            except AttributeError:
                logging.info(f"Value of 'line' can't be {line}")
    return diagnostics


def least_frequent(diagnostic_column: List) -> str:
    return min(set(diagnostic_column), key=diagnostic_column.count)


def most_frequent(diagnostic_column: List) -> str:
    return max(set(diagnostic_column), key=diagnostic_column.count)


def build_diagnostic_columns(diagnostics: List) -> List:
    diagnostic_columns = []

    for diagnostic in diagnostics:
        if len(diagnostic_columns) < len(diagnostic):
            for c in diagnostic:
                diagnostic_columns.append([c])
        else:
            for i, c in enumerate(diagnostic):
                diagnostic_columns[i].append(c)

    return diagnostic_columns


def build_gamma_rate(diagnostic_columns: List) -> str:
    gamma_rate = ""

    for diagnostic_column in diagnostic_columns:
        gamma_rate = gamma_rate + most_frequent(diagnostic_column=diagnostic_column)

    return gamma_rate


def build_epsilon_rate(diagnostic_columns: List) -> str:
    epsilon_rate = ""

    for diagnostic_column in diagnostic_columns:
        epsilon_rate = epsilon_rate + least_frequent(diagnostic_column=diagnostic_column)

    return epsilon_rate


def get_part1_answer(diagnostics: List) -> int:
    diagnostic_columns = build_diagnostic_columns(diagnostics=diagnostics)
    gamma_rate = build_gamma_rate(diagnostic_columns=diagnostic_columns)
    epsilon_rate = build_epsilon_rate(diagnostic_columns=diagnostic_columns)
    return int(gamma_rate, 2) * int(epsilon_rate, 2)


def extract_bit_criteria(diagnostics: List, func: Callable, i: int = 0) -> str:
    diagnostic_columns = build_diagnostic_columns(diagnostics=diagnostics)

    # get func bit in position i
    bit_val = func(diagnostic_column=diagnostic_columns[i])
    filtered_diagnostics = list(filter(lambda diagnostic: diagnostic[i] == bit_val, diagnostics))

    if len(filtered_diagnostics) == 1:
        return filtered_diagnostics[0]

    return extract_bit_criteria(diagnostics=filtered_diagnostics, func=func, i=i+1)


def extract_oxygen_generator_rating(diagnostics: List) -> str:

    def oxygen_generator_frequency(diagnostic_column: List):
        counter_list = Counter(diagnostic_column).most_common()
        if len(counter_list) > 1:
            if counter_list[0][1] == counter_list[1][1]:
                return "1"
        return counter_list[0][0]

    return extract_bit_criteria(diagnostics=diagnostics, func=oxygen_generator_frequency)


def extract_co2_scrubber_rating(diagnostics: List) -> str:

    def co2_scrubber_frequency(diagnostic_column: List):
        counter_list = Counter(diagnostic_column).most_common()
        if len(counter_list) > 1:
            if counter_list[0][1] == counter_list[1][1]:
                return "0"
        return counter_list[1][0]

    return extract_bit_criteria(diagnostics=diagnostics, func=co2_scrubber_frequency)


def get_part2_answer(diagnostics: List) -> int:
    oxygen_generator_rating = extract_oxygen_generator_rating(diagnostics=diagnostics)
    co2_scrubber_rating = extract_co2_scrubber_rating(diagnostics=diagnostics)
    return int(oxygen_generator_rating, 2) * int(co2_scrubber_rating, 2)


def main():
    logging.basicConfig(filename=LOG_FILENAME,
                        level=logging.getLevelName(LOGGING_LEVEL),
                        format=LOG_FORMAT)

    diagnostics = extract_diagnostics(filename=INPUT_FILENAME)
    print(f"Part 1 Answer: {get_part1_answer(diagnostics=diagnostics)}")
    print(f"Part 2 Answer: {get_part2_answer(diagnostics=diagnostics)}")


if __name__ == "__main__":
    main()
