import logging
import os
import sys
from typing import List

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")
LOG_FILENAME = os.getenv("LOG_FILENAME")
LOG_FORMAT = os.getenv("LOG_FORMAT")
LOGGING_LEVEL = os.getenv("LOGGING_LEVEL")


def extract_crabs(filename: str) -> List:
    crabs = []

    with open(filename) as f:
        for line in f.readlines():
            try:
                scrubbed_line = line.strip("\n")
                crabs = [int(x) for x in scrubbed_line.split(",")]
            except ValueError:
                logging.info(f"Couldn't convert value {scrubbed_line}")
            except AttributeError:
                logging.info(f"Value of 'line' can't be {line}")

    return crabs


def calculate_fuel_usage(crabs: List, part2: bool) -> int:
    lowest_fuel_usage = sys.maxsize

    # iterate from the minimum value to the maximum value found in crabs to test every position
    for i in range(min(crabs), max(crabs) + 1):
        # get the base position change between each crab's current position and the target position
        fuel_usage = [abs(crab - i) for crab in crabs]

        # part 2 uses a sequence addition method to calculate fuel usage, this sums each crab fuel usage from 0 to n
        if part2:
            fuel_usage = [int((fuel * (fuel + 1)) / 2) for fuel in fuel_usage]
        fuel_usage = sum(fuel_usage)

        if lowest_fuel_usage > fuel_usage:
            lowest_fuel_usage = fuel_usage

    return lowest_fuel_usage


def get_part1_answer(crabs: List):
    return calculate_fuel_usage(crabs=crabs, part2=False)


def get_part2_answer(crabs: List):
    return calculate_fuel_usage(crabs=crabs, part2=True)


def main():
    logging.basicConfig(filename=LOG_FILENAME,
                        level=logging.getLevelName(LOGGING_LEVEL),
                        format=LOG_FORMAT)

    crabs = extract_crabs(filename=INPUT_FILENAME)
    print(f"Part 1 Answer: {get_part1_answer(crabs=crabs)}")
    print(f"Part 2 Answer: {get_part2_answer(crabs=crabs)}")


if __name__ == "__main__":
    main()
