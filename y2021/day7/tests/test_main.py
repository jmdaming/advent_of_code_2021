import os

from y2021.day7.main import extract_crabs, get_part1_answer, get_part2_answer

from dotenv import load_dotenv

load_dotenv()
INPUT_FILENAME = os.getenv("INPUT_FILENAME")


def test_extract_moves_and_boards_len():
    crabs = extract_crabs(filename=INPUT_FILENAME)
    assert len(crabs) == 10


def test_get_part1_answer():
    crabs = extract_crabs(filename=INPUT_FILENAME)
    assert get_part1_answer(crabs=crabs) == 37


def test_get_part2_answer():
    crabs = extract_crabs(filename=INPUT_FILENAME)
    assert get_part2_answer(crabs=crabs) == 168
