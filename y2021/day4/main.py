import logging
import os
import re
from typing import List, Tuple

from dotenv import load_dotenv

load_dotenv()
BOARD_WIDTH = os.getenv("BOARD_WIDTH")
BOARD_HEIGHT = os.getenv("BOARD_HEIGHT")
INPUT_FILENAME = os.getenv("INPUT_FILENAME")
LOG_FILENAME = os.getenv("LOG_FILENAME")
LOG_FORMAT = os.getenv("LOG_FORMAT")
LOGGING_LEVEL = os.getenv("LOGGING_LEVEL")


class Board(object):

    def __init__(self, values=None):
        if values is None:
            values = []
        self.values = values

    def play_move(self, move):
        board_values = self.values
        for row in board_values:
            for val in row:
                if val["value"] == move:
                    val["found"] = True
        self.values = board_values
        return self.check_for_win_condition()

    @staticmethod
    def __check_for_win(board_values):
        for row in board_values:
            found_count = 0
            for val in row:
                if val["found"]:
                    found_count = found_count + 1
                    if found_count == len(row):
                        return True
        return False

    def check_rows_for_win(self):
        board_values = self.values
        return self.__check_for_win(board_values=board_values)

    def check_columns_for_win(self):
        board_values = \
            [col for col in [list(v) for v in zip(*[col[0] for col in zip(lst[0:] for lst in self.values[0:])])]]
        return self.__check_for_win(board_values=board_values)

    def check_for_win_condition(self):
        return self.check_rows_for_win() or self.check_columns_for_win()

    def get_score_of_board(self):
        unmarked_sum = 0
        for row in self.values:
            for val in row:
                if not val["found"]:
                    unmarked_sum = unmarked_sum + val["value"]
        return unmarked_sum


def extract_moves_and_boards(filename: str) -> Tuple[List, List]:
    moves = []
    boards = []

    with open(filename) as f:
        first_line = False
        board = None
        for line in f.readlines():
            try:
                scrubbed_line = line.strip("\n")
                if not first_line:
                    moves = [int(x) for x in scrubbed_line.split(",")]
                    first_line = True
                else:
                    if scrubbed_line == "":
                        if board:
                            boards.append(board)
                        board = Board()
                    else:
                        board.values.append(
                            [{"value": int(x), "found": False} for x in re.split(r"\s+", scrubbed_line.strip())])
            except ValueError:
                logging.info(f"Couldn't convert value {scrubbed_line}")
                print(f"Couldn't convert value {scrubbed_line}")
            except AttributeError:
                logging.info(f"Value of 'line' can't be {line}")
        boards.append(board)

    return moves, boards


def get_part1_answer(moves: List, boards: List):
    for move in moves:
        for board in boards:
            if board.play_move(move=move):
                return board.get_score_of_board() * move


def get_part2_answer(moves: List, boards: List):
    for move in moves:
        for board in boards:
            if board.play_move(move=move):
                if len(boards) == 1:
                    return board.get_score_of_board() * move
                else:
                    filter_boards = boards
                    filter_boards.remove(board)
                    return get_part2_answer(moves, filter_boards)


def main():
    logging.basicConfig(filename=LOG_FILENAME,
                        level=logging.getLevelName(LOGGING_LEVEL),
                        format=LOG_FORMAT)

    moves, boards = extract_moves_and_boards(filename=INPUT_FILENAME)
    print(f"Part 1 Answer: {get_part1_answer(moves=moves, boards=boards)}")
    print(f"Part 2 Answer: {get_part2_answer(moves=moves, boards=boards)}")


if __name__ == "__main__":
    main()
