import os

from y2021.day4.main import Board, extract_moves_and_boards, get_part1_answer, get_part2_answer

from dotenv import load_dotenv

load_dotenv()
BOARD_WIDTH = int(os.getenv("BOARD_WIDTH"))
BOARD_HEIGHT = int(os.getenv("BOARD_HEIGHT"))
INPUT_FILENAME = os.getenv("INPUT_FILENAME")


def test_extract_moves_and_boards_len():
    moves, boards = extract_moves_and_boards(filename=INPUT_FILENAME)
    assert len(moves) == 27
    assert len(boards) == 3
    assert len(boards[0].values) == BOARD_HEIGHT
    assert len(boards[0].values[0]) == BOARD_WIDTH


def test_get_part1_answer():
    moves, boards = extract_moves_and_boards(filename=INPUT_FILENAME)
    assert get_part1_answer(moves=moves, boards=boards) == 4512


def test_get_part2_answer():
    moves, boards = extract_moves_and_boards(filename=INPUT_FILENAME)
    assert get_part2_answer(moves=moves, boards=boards) == 1924
